import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable } from 'rxjs';
import { Lobo } from './lobo';

@Injectable({
  providedIn: 'root'
})
export class LoboService {
  baseUrl = 'https://lobinhos-api.herokuapp.com/'

  constructor(
    private http: HttpClient
  ) { }

  getWolf(){
    return this.http.get(this.baseUrl + 'wolves');
  }
}
