import { TestBed } from '@angular/core/testing';

import { LoboService } from './lobo.service';

describe('LoboService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoboService = TestBed.get(LoboService);
    expect(service).toBeTruthy();
  });
});
