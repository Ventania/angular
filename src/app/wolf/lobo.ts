export class Lobo {
    constructor(
        public name: string,
        public age: number,
        public description: string,
        public photo: string
    ){}
}
