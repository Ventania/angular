import { Component, OnInit } from '@angular/core';
import { LoboService } from '../lobo.service';
import { Lobo } from '../lobo';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wolf-list',
  templateUrl: './wolf-list.component.html',
  styleUrls: ['./wolf-list.component.scss']
})
export class WolfListComponent implements OnInit {
  wolf = [];
  constructor(
    private loboService: LoboService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loboService.getWolf().subscribe(
      (data: Lobo[]) => {
        this.wolf = data.map(info => new Lobo(
          info['name'],
          +info['age'],
          info['description'],
          info['photo']
        ))
      }
    )
  }
  goToShow(id:number){
    this.router.navigate(['/wolf',id])
  }
}
