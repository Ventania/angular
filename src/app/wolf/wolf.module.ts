import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WolfListComponent } from './wolf-list/wolf-list.component';
import { WolfService } from './wolf.service';
import { RouterModule, Route } from '@angular/router';
import { WolfShowComponent } from './wolf-show/wolf-show.component';

const routes: Route[] = [{
  path: 'wolf/:id', component: WolfShowComponent
},{
  path: 'wolf', component: WolfListComponent
}]

@NgModule({
  declarations: [
    WolfListComponent,
    WolfShowComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    WolfService
  ],
  exports: [
    WolfListComponent
  ]})
export class WolfModule { }
