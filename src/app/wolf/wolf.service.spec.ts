import { TestBed } from '@angular/core/testing';

import { WolfService } from './wolf.service';

describe('WolfService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WolfService = TestBed.get(WolfService);
    expect(service).toBeTruthy();
  });
});
