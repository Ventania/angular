import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { WolfService } from './wolf/wolf.service';
import { WolfListComponent } from './wolf/wolf-list/wolf-list.component';
import { WolfModule } from './wolf/wolf.module';
import {HttpClientModule} from '@angular/common/http'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    WolfModule,
    BrowserModule,
    AppRoutingModule,
    UiModule
  ],
  providers: [
    WolfService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
