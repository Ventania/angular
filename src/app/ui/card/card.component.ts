import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  title: string = "Sou Titulo";
  link: string = "www.google.com";
  showCard: boolean = true
  constructor() { 
  }

  ngOnInit() {

  }
  
  updateLink(): void{
    this.link = 'https://facebook.com'

    if (this.showCard) {
      this.showCard = false;
    }else{
      this.showCard = true;
    }
  }
}
