import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { RouterModule, Route } from '@angular/router';

const routes: Route[] = [{
  path: 'Card', component: CardComponent
}];

@NgModule({
  declarations: [
    CardComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[
    CardComponent
  ],
})
export class UiModule { }
