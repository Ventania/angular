import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WolfListComponent } from './wolf/wolf-list/wolf-list.component';
import { CardComponent } from './ui/card/card.component';


const routes: Routes = [,{
  path: 'Card', component: CardComponent
},{
  path: '', component: WolfListComponent
}, {
  path: '**', redirectTo: ''
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
